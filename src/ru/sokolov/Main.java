package ru.sokolov;

import ru.sokolov.util.Decoder;
import ru.sokolov.util.Encoder;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean stop = false;
        while (!stop) {
            try {
                System.out.println("Выберите действие (1 - шифрование, 2 - расшифровка, 3 - выйти из программы) :");
                int type = scanner.nextInt();
                if (type == 3) {
                    stop = true;
                    continue;
                }
                scanner.nextLine();
                System.out.println("Введите ключ (5-ти значное число): ");
                String keyStr = scanner.nextLine();
                if (keyStr.length() > 5) {
                    throw new IOException("Ключ должен быть пятизначным числом!");
                }
                int key = Integer.parseInt(keyStr);
                System.out.println("Введите сообщение : ");
                String message = scanner.nextLine();
                if (type == 1) {
                    Encoder encoder = new Encoder();
                    System.out.println("Ваше зашифрованное сообщение : " + encoder.encode(message, key));
                } else if (type == 2) {
                    Decoder decoder = new Decoder();
                    System.out.println("Ваше расшифрованное сообщение : " + decoder.decode(message, key));
                } else {
                    stop = true;
                }
            } catch (Exception e) {
                System.out.println("Произошла ошибка! " + e.getMessage());
            }
        }
    }
}
