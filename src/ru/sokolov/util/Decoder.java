package ru.sokolov.util;

public class Decoder {
    private final Encoder encoder = new Encoder();

    public String decode(String message, int key) {
        int[] b = new int[5];
        for (int i = 0; i < 5; i++) {
            b[i] = key % 10;
            key = key / 10;
        }
        String result = "";
        for (int i = 0; i < 5; i++) {
            if (i == 0) {
                result = decodeCaesar(message, b[i]);
            } else {
                result = decodeCaesar(result, b[i]);
            }
        }
        return result;
    }

    public String decodeCaesar(String message, int offset) {
        return encoder.encodeCaesar(message, 26 - (offset % 26));
    }
}
