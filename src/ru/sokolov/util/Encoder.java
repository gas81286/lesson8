package ru.sokolov.util;

public class Encoder {

    public String encode(String message, int key) {
        int[] b = new int[5];
        for (int i = 0; i < 5; i++) {
            b[i] = key % 10;
            key = key / 10;
        }
        String result = "";
        for (int i = 0; i < 5; i++) {
            if (i == 0) {
                result = encodeCaesar(message, b[i]);
            } else {
                result = encodeCaesar(result, b[i]);
            }
        }
        return result;
    }

    public String encodeCaesar(String message, int offset) {
        StringBuilder result = new StringBuilder();
        for (char c : message.toCharArray()) {
            if (c != ' ') {
                int originalAlphabetPosition = c - 'a';
                int newAlphabetPosition = (originalAlphabetPosition + offset) % 26;
                char newCharacter = (char) ('a' + newAlphabetPosition);
                result.append(newCharacter);
            } else {
                result.append(c);
            }
        }
        return result.toString();
    }
}
